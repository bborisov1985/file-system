import './App.css'
import { useState } from 'react'
import nodesData from './data/nodesData'
import SearchBar from './components/SerachBar/SearchBar'
import FileManager from './components/FileManager/FileManager'
import VirtualizedData from './components/VirtualizedData/VirtualizedData'

const App = () => {

  const [nodes, setNodes] = useState(nodesData);

  return (
    <div className="file-system">
      <div className="container">
        <SearchBar
          data={nodes}
          search={setNodes}
        />
        <FileManager
          data={nodes}
          onChange={setNodes} />
        <VirtualizedData />
      </div>
    </div>
  );
}

export default App;
