const nodesData = [
    {
        id: 1,
        name: "src",
        type: "folder",
        children: [
            {
                id: 2,
                name: "assets",
                type: "folder",
                children: [
                    {
                        id: 3,
                        name: "fonts",
                        type: "folder",
                        children: [
                            {
                                id: 4,
                                name: "FinancierDisplayWeb-Bold.woff2",
                                type: "file",
                                children: [],
                            },
                            {
                                id: 5,
                                name: "FinancierDisplayWeb-Regular.woff2",
                                type: "file",
                                children: [],
                            },
                            {
                                id: 6,
                                name: "MetricWeb-Regular.woff2",
                                type: "file",
                                children: [],
                            },
                            {
                                id: 7,
                                name: "MetricWeb-Semibold.woff2",
                                type: "file",
                                children: [],
                            }
                        ],
                    },
                    {
                        id: 8,
                        name: "icons",
                        type: "folder",
                        children: [],
                    },
                    {
                        id: 9,
                        name: "images",
                        type: "folder",
                        children: [],
                    },
                    {
                        id: 10,
                        name: "styles",
                        type: "folder",
                        children: [],
                    }
                ],
            },
            {
                id: 11,
                name: "components",
                type: "folder",
                children: [],
            }
        ]
    }
]

export default nodesData;