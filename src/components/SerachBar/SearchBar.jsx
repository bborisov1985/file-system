import { useState } from 'react'
import './SearchBar.css'

const SearchBar = ({ search, data }) => {

    const [searchNode, setSearchNode] = useState('');
    const traverseData = (data) => data.filter(el => el.name.toLowerCase().includes(searchNode) || traverseData(el.children).length)

    const searchForNode = () => {    
        search(traverseData(data));   
        setSearchNode('');
    }

    return (
        <div className="searchbar-container">
            <input id="search-input" type="text" value={searchNode} placeholder="Type here..." onChange={(e) => setSearchNode(e.target.value)} />
            <button className="search-button" onClick={searchForNode}>Search</button>
        </div>
    )
}

export default SearchBar

