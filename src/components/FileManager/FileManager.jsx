import React from 'react'
import SingleNode from '../SingleNode/SingleNode';

const FileManager = ({ data, onChange }) => {

    const createNode = (nodeData, path) => {

        const traverseData = (data, path) => {

            if (path.length === 1 && data.length) {
                const index = path[0];
                const element = { ...data[index], children: [...data[index].children, nodeData] };
                return data.map((e, i) => i === index ? element : e)
            } else {
                return data.map(el => ({ ...el, children: traverseData(el.children, path.slice(1)) }));
            }
        }

        onChange(traverseData(data, path))

    }

    const removeNode = (path) => {
        const traverseData = (data, path) => {

            if (path.length === 1 && data.length) {
                const index = path[0];
                return data.filter((e, i) => i !== index)
            } else {
                return data.map(el => ({ ...el, children: traverseData(el.children, path.slice(1)) }));
            }
        }

        onChange(traverseData(data, path))

    }

    return (
        <div>
            {data.map((node, i) => {
                const path = [i];
                return (
                    <SingleNode
                        {...node}
                        path={path}
                        key={path.toString() + node.name}
                        handleCreate={createNode}
                        remove={removeNode}
                    >
                    </SingleNode>
                )
            })}
        </div>
    )
}

export default FileManager
