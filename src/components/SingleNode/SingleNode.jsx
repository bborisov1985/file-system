import { useState } from 'react'
import CreateNode from '../CreateNode/CreateNode';
import DeleteNode from '../DeleteNode/DeleteNode';
import './SignleNode.css'

const SingleNode = ({ name, type, children, handleCreate, remove, path }) => {

    const [showChildren, toggleShowChildren] = useState(true);
    const [showAddDialog, toggleShowAddDialog] = useState(false);
    const [showDeleteDialog, toggleShowDeleteDialog] = useState(false);

    return (
        <div>
            {type === 'folder'
                ? <div className="single-node-container">
                    <div className="single-node-name folder" onClick={() => toggleShowChildren(prev => !prev)}>
                        {name}
                    </div>
                    <div>
                        <button className="single-node-button" onClick={() => toggleShowAddDialog(prev => !prev)}>Add</button>
                        <button className="single-node-button" onClick={() => toggleShowDeleteDialog(prev => !prev)}>Delete</button>
                    </div>
                </div>
                : <div className="single-node-container">
                    <div className="single-node-name file" >
                        {name}
                    </div>
                    <div>
                        <button className="single-node-button" onClick={() => toggleShowDeleteDialog(prev => !prev)}>Delete</button>
                    </div>
                </div>}
            {showDeleteDialog
                && <DeleteNode remove={() => remove(path)} toggle={toggleShowDeleteDialog} />
            }
            {showAddDialog
                && <CreateNode
                    onCreate={(newNode) => {
                        handleCreate(newNode, path)
                        toggleShowAddDialog(prev => !prev)
                    }}
                    onCancel={toggleShowAddDialog} />
            }
            {showChildren && type === 'folder'
                && <div className="node-child">
                    {children.length
                        ? [...children]
                            .sort((a, b) => a.name.localeCompare(b.name))
                            .map((child, i) => {
                                const childPath = [...path, i];
                                return (
                                    <SingleNode
                                        {...child}
                                        path={childPath}
                                        key={childPath.toString() + child.name}
                                        handleCreate={handleCreate}
                                        remove={remove}
                                    >
                                    </SingleNode>
                                )
                            })
                        : <p className="empty-folder">The folder is empty</p>
                    }
                </div>}
        </div >
    )
}

export default SingleNode

