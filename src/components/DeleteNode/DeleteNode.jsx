import './DeleteNode.css'

const DeleteNode = ({ remove, toggle }) => {
    return (
        <div className="delete-node">
            <div className="delete-node-container">
                <p>Are you sure you want to delete this node?</p>
                <div className="delete-node-buttons">
                    <button className="delete-node-button" onClick={remove}>Delete</button>
                    <button className="delete-node-button" onClick={() => toggle(prev => !prev)}>Cancel</button>
                </div>
            </div>
        </div>
    )
}

export default DeleteNode
