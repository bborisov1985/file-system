import { useState } from 'react'
import './CreateNode.css'

const CreateNode = ({ onCreate, onCancel }) => {

    const [name, setName] = useState('');
    const [type, setType] = useState('');

    const createNode = () => {
        onCreate({ name, type, children: [] })
        setName('');
        setType('');
    }

    return (
        <div className="create-node">
            <div className="create-node-container">
                <div className="create-node-name">
                    <label>Name:</label>
                    <input id="input" type="text" value={name} placeholder="Type here..." onChange={(e) => setName((prev) => e.target.value)} />
                </div>
                <div className="create-node-radio">
                    <p>Type:</p>
                    <input id="Folder" type="radio" name="type" value="folder" onChange={(e) => setType(e.target.value)} />
                    <label htmlFor="Folder">Folder</label>
                    <input id="File" type="radio" name="type" value="file" onChange={(e) => setType(e.target.value)} />
                    <label htmlFor="File">File</label>
                </div>
                <div className="create-node-buttons">
                    <button className="single-node-button" onClick={createNode}>Add</button>
                    <button className="single-node-button" onClick={() => onCancel(prev => !prev)}>Cancel</button>
                </div>
            </div>
        </div>
    )
}

export default CreateNode
