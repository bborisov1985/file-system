import { useState, useEffect, useRef } from 'react'
import { List, AutoSizer, CellMeasurer, CellMeasurerCache } from 'react-virtualized'
import faker from 'faker'
import './VirtualizedData.css'


const VirtualizedData = () => {

    const cache = useRef(new CellMeasurerCache({
        fixedWidth: true,
        defaultHeight: 100,
    }))
    const [data, setData] = useState([]);

    useEffect(() => {
        setData(
            [...Array(1000).keys()].map(key => {
                return {
                    id: key,
                    name: `${faker.name.firstName()} ${faker.name.lastName()}`,
                    bio: faker.lorem.lines(Math.random() * 10),
                }
            })
        )
    }, [])

    return (
        <div>
            <div className="virtualized-item">
                <AutoSizer>
                    {({ width, height }) => (
                        <List
                            width={width}
                            height={height}
                            rowHeight={cache.current.rowHeight}
                            deferredMeasurementCache={cache.current}
                            rowCount={data.length}
                            rowRenderer={({ key, index, style, parent }) => {
                                const person = data[index];
                                return (
                                    <CellMeasurer
                                        key={key}
                                        cache={cache.current}
                                        parent={parent}
                                        columnIndex={0}
                                        rowIndex={index}>
                                        <div style={style}>
                                            <h2>{person.name}</h2>
                                            <p>{person.bio}</p>
                                        </div>
                                    </CellMeasurer>
                                )
                            }}
                        />
                    )}
                </AutoSizer>
            </div>
        </div>
    )
}

export default VirtualizedData

